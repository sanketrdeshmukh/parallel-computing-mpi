#include <iostream>
#include <mpi.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

using namespace std;

void my_bcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator) 
{
	int local_rank, local_nproc;

  	MPI_Comm_rank(communicator, &local_rank);
  	MPI_Comm_size(communicator, &local_nproc);
  	MPI_Request request[100];
	MPI_Status status[100];
	
	// Need to optimize the following piece of code - looks like a dead cat
	int tracker=0;
	for (int i = 0; i < local_nproc; ++i)
	{
		if(local_rank == root & i != root) 
		{
			MPI_Isend(data, count, MPI_INT, i, 1, communicator, &request[tracker]);
			tracker++;
		}

	}

	for(int i =0; i <local_nproc; ++i)
	{
		if(local_rank != root & i != root)
		{
			MPI_Irecv(data, count, MPI_INT, root, 1, communicator, &request[tracker]);
			tracker++;
		}
	}
	MPI_Barrier(communicator);
}


int main(int argc, char **argv)
{
	int rank, nproc, err;
	


	int num_elements = atoi(argv[1]);
  	int num_trials = atoi(argv[2]);
	
	double total_my_bcast_time = 0.0;
	double total_mpi_bcast_time = 0.0;

	float* data = (float*)malloc(sizeof(float) * num_elements);
	assert(data != NULL);

	srand (time(NULL));

	for (int i = 0; i < num_elements; ++i)
	{
		data[i] = rand();
		// You can check the array is not of same value
		//cout << data[i] << endl;
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	for (int i = 0; i < num_trials; ++i)
	{
		/* code */
	
		MPI_Barrier(MPI_COMM_WORLD);
		// my_bcast
		total_my_bcast_time -= MPI_Wtime();
		my_bcast(data, num_elements, MPI_FLOAT, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
	    total_my_bcast_time += MPI_Wtime();

	    // mpi_bcast
		total_mpi_bcast_time -= MPI_Wtime();
		MPI_Bcast(data, num_elements, MPI_FLOAT, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		total_mpi_bcast_time += MPI_Wtime();
	}

    if (rank == 0)
    {

    	ofstream myfile;
    	myfile.open ("output.txt",  std::ofstream::out | std::ofstream::app);
    	//myfile << "Writing this to a file.\n";
  		myfile << "No. of nodes : " << nproc << endl;
    	myfile << "Avg Time for my_bcast : " << total_my_bcast_time/num_trials << " , num_elements : " << num_elements 
  		<< " , num_trials : " << num_trials << endl;
    	myfile << "Avg Time for mpi_bcast : " << total_mpi_bcast_time/num_trials<< " , num_elements : " << num_elements 
    	<< " , num_trials : " << num_trials <<endl;
		myfile << "Efficiency : " << (total_my_bcast_time)/total_mpi_bcast_time << endl;
		myfile.close();
    }
    
    MPI_Finalize();
	return 0;
}
