#include <iostream>
#include <mpi.h>

using namespace std;

int main (int argc, char *argv[])
{

	int numtasks, rank, next, prev, buf[2],tag1=1, tag2=2, a=-99;

	MPI_Request reqs[4];
	MPI_Status stats[4];
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	prev = rank-1; next = rank+1;
	
	if (rank == 0) 
	{
		prev = numtasks - 1;
		a = 99;
	}

	if (rank == (numtasks - 1)) next = 0;

	cout << "The value of a : " << a << " in proc " << rank << endl;
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Isend(&a, 1, MPI_INT, prev, tag2, MPI_COMM_WORLD, &reqs[2]);
	MPI_Isend(&a, 1, MPI_INT, next, tag1, MPI_COMM_WORLD, &reqs[3]);


	MPI_Irecv(&a, 1, MPI_INT, prev, tag1, MPI_COMM_WORLD, &reqs[0]);
	MPI_Irecv(&a, 1, MPI_INT, next, tag2, MPI_COMM_WORLD, &reqs[1]);
		
	MPI_Waitall(4, reqs, stats);
	
	//cout << "Task " << rank << " communicated with tasks " << prev << " & " << next <<endl;
	cout << "The value of a : " << a << " in proc " << rank << endl;	
	MPI_Finalize();
}