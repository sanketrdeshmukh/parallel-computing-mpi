#!/bin/sh

echo '===================================='
echo 'compile and run commands below'
echo '===================================='

make clean
make all

echo '===================================='
echo 'output of your program starts below'
echo '===================================='

mpirun -np $1 ./run