#include<iostream>
#include<mpi.h>

using namespace std;
int main(int argc, char **argv)
{

	int nproc, rank; 
	int a=-99, errorcode;

	MPI_Request requestS[100];
    MPI_Status status[100];

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    if(rank == 0)
    {
    	a = 99;
    	for (int i = 1; i < nproc; ++i)
    	{
    		MPI_Isend(&a, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &requestS[i]);	
    		//MPI_Wait(&requestS[i], &status[i]);
    		//cout << "message sent to : " << i << endl;
    	}
    	 //MPI_Waitall(nproc-1, requestS, status);
    	//cout << "Value of a: " << a <<" in proc " << rank <<endl;
    }
    else
    {
    	for (int i = 1; i < nproc; ++i)
    	{
    		/* code */
    		errorcode = MPI_Irecv(&a, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &requestS[i+nproc-1]);
    		//errorcode = MPI_Wait(&requestS[i], &status[i]);
    		//cout << "Value of a: " << a <<" in proc " << rank <<endl;

    	}
    	MPI_Waitall(2*nproc-2, requestS, status);
    }

	// for (int i = 1; i < nproc; i++)
	// {
	// 	/* code */
	// 	cout << i <<endl;
	// 	if(rank == 0)
	// 	{
	// 		a=99;
	// 		MPI_Isend(&a, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &requestS[i]);
	// 		//errorcode = MPI_Wait(&requestS[i], &status[i]);
	// 	}
	// 	else
	// 	{
	// 		errorcode = MPI_Irecv(&a, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &requestS[i]);
	// 		errorcode = MPI_Wait(&requestS[i], &status[i]);
	// 		cout << "Value of a: " << a <<" in proc " << rank <<endl;
	// 	}
	// }


    MPI_Abort(MPI_COMM_WORLD, errorcode);
    MPI_Finalize();
}