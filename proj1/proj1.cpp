#include<iostream>
#include<mpi.h>
using namespace std;
int main(int argc, char **argv)
{
	int rank, npes;
	//Using it as an Index tracker
	int tracker=0;

	//Initialize MPI
	MPI_Init(&argc, &argv);
	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &npes);	
	
	// using it as a tracker for comm
	int number=0;

	//Using rank 0 as master
	if(rank==0)
	{
		cout<<"Hello from processor " << rank <<endl;
		// MPI_Barrier(MPI_COMM_WORLD);	
		while (tracker < npes-1)
		{
			tracker++;
			MPI_Recv(&number, 1, MPI_INT, tracker, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			cout<< "Hello from processor " << number << endl;
			//MPI_Barrier(MPI_COMM_WORLD);
		}		
	}

	// for the slaves to communicate with master
	else
	{
		number	= rank;
		MPI_Send(&number, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}
	
	MPI_Finalize();
}
