#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <ctime>

using namespace std;

int main(int argc, char **argv)
{
	int N = atoi(argv[1]);
	int i, j;
	double a[N][N], b[N][N], c[N][N];

	srand(time(NULL));

	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			a[i][j] = 2*(rand()/double(RAND_MAX))-1;

	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			b[i][j] = 2*(rand()/double(RAND_MAX))-1;

	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			c[i][j] = 0;

	clock_t begin = clock();

	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			for (int k = 0; k < N; k++)
				c[i][j] += a[i][k]*b[k][j];

	clock_t end = clock();
  	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

  	cout << elapsed_secs << endl;

    ofstream myfile;
    myfile.open ("output.txt",  std::ofstream::out | std::ofstream::app);
    myfile << " Time for serial multiplication : " << elapsed_secs << " , num_elements : " << N << endl;
    myfile.close();
}
