#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <fstream>

using namespace std;

#define MASTER 0
#define FROM_MASTER 1
#define FROM_WORKER 2

int main (int argc, char **argv)
{
    int N = atoi(argv[1]);
    int	numtasks,
	taskid,
	numworkers,
	source,
	dest,
	mtype,
	rows,
	averow, extra, offset,
	i, j, k, rc;
    double	a[N][N],
	b[N][N],
	c[N][N];
    double mult_time;
    MPI_Status status;

    srand (time(NULL));

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    numworkers = numtasks-1;


/**************************** master task ************************************/
   if (taskid == MASTER)
   {
      for (i=0; i<N; i++)
         for (j=0; j<N; j++)
            a[i][j]= 2*(rand()/double(RAND_MAX))-1;
      for (i=0; i<N; i++)
         for (j=0; j<N; j++)
            b[i][j]= 2*(rand()/double(RAND_MAX))-1;

      
      averow = N/numworkers;
      extra = N%numworkers;
      offset = 0;
      mtype = FROM_MASTER;

      
      mult_time -= MPI_Wtime();

      for (dest=1; dest<=numworkers; dest++)
      {
         rows = (dest <= extra) ? averow+1 : averow;   	
         
         MPI_Send(&offset, 1, MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&rows, 1, MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&a[offset][0], rows*N, MPI_DOUBLE, dest, mtype,
                   MPI_COMM_WORLD);
         MPI_Send(&b, N*N, MPI_DOUBLE, dest, mtype, MPI_COMM_WORLD);
         offset = offset + rows;
      }

      
      mtype = FROM_WORKER;
      for (i=1; i<=numworkers; i++)
      {
         source = i;
         MPI_Recv(&offset, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&rows, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&c[offset][0], rows*N, MPI_DOUBLE, source, mtype, 
                  MPI_COMM_WORLD, &status);
      }

      
      mult_time += MPI_Wtime();
      cout << mult_time << endl;
      ofstream myfile;
      myfile.open ("output.txt",  std::ofstream::out | std::ofstream::app);
      myfile << " Time for multiplication : " << mult_time << " , num_elements : " << N 
        << " , num_processes : " << numtasks << endl;
      myfile.close();

N      // /* Print results */
      // for (i=0; i<N; i++)
      // {
      //   cout << endl;
      //   for (j=0; j<N; j++)
      //       cout << a[i][j] << " ";
      // }
   }


/**************************** worker task ************************************/
   if (taskid > MASTER)
   {
      mtype = FROM_MASTER;
      MPI_Recv(&offset, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&rows, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&a, rows*N, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&b, N*N, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);

      for (k=0; k<N; k++)
         for (i=0; i<rows; i++)
         {
            c[i][k] = 0.0;
            for (j=0; j<N; j++)
               c[i][k] = c[i][k] + a[i][j] * b[j][k];
         }
      mtype = FROM_WORKER;
      MPI_Send(&offset, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&rows, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&c, rows*N, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
   }
   MPI_Finalize();
}