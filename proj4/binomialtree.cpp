#include <iostream>
#include <math.h>
#include <mpi.h>

using namespace std;

int main(int argc, char **argv)
{
	/* Variables */
	int rank, nproc, masklow, maskhigh, sendnode, recvnode;
	bool recvFlag=0;
	int a=0;
	MPI_Request request[100];
	MPI_Status status[100];
	int tracker = 0;

	/* code */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);

	if (rank==0)
	{
		a = 99;
		recvFlag = 1;	
	}
	cout << a <<" at rank : " << rank << " BEFORE" << endl;

	for (int i = 0; i < 4; ++i)
	{
		masklow = pow(2,i);
		maskhigh = pow(2,i+1);
		// for (int j = 0; j < mask; ++j)
		// {
			// if (rank == j) MPI_Send(&a, 1, MPI_INT, mask+j, 0, MPI_COMM_WORLD);
			// if (rank == mask+j) MPI_Recv(&a, 1, MPI_INT, j, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); 
		if (rank < masklow) MPI_Isend(&a, 1, MPI_INT, masklow+rank, 0, MPI_COMM_WORLD, &request[tracker]);
		tracker++;
		if (rank >= masklow && rank < maskhigh) MPI_Irecv(&a, 1, MPI_INT, rank - masklow, 0, MPI_COMM_WORLD, &request[tracker]);
		tracker++;
			// MPI_Barrier(MPI_COMM_WORLD);
		// }
		MPI_Barrier(MPI_COMM_WORLD);
	}
	// MPI_Barrier(MPI_COMM_WORLD);
	cout << a <<" at rank : " << rank << " AFTER" << endl;

	MPI_Finalize();

	return 0;
}