#include <iostream>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

#define NODES 16
#define RUNS 10
using namespace std;

void my_bcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator) 
{
	int local_rank, local_nproc;

  	MPI_Comm_rank(communicator, &local_rank);
  	MPI_Comm_size(communicator, &local_nproc);
  	MPI_Request request[100];
	MPI_Status status[100];
	
	// Need to optimize the following piece of code - looks like a dead cat
	int tracker=0;
	for (int i = 0; i < local_nproc; ++i)
	{
		if(local_rank == root & i != root) 
		{
			MPI_Isend(data, count, MPI_INT, i, 1, communicator, &request[tracker]);
			tracker++;
		}

	}

	for(int i =0; i <local_nproc; ++i)
	{
		if(local_rank != root & i != root)
		{
			MPI_Irecv(data, count, MPI_INT, root, 1, communicator, &request[tracker]);
			tracker++;
		}
	}
	MPI_Barrier(communicator);
}

int main(int argc, char **argv)
{
	/* code */
	int rank, nproc, reorder=0, N;
	N = atoi(argv[1]);
	MPI_Comm graph_comm;
	double mpi_bcast_time, my_bcast_time;

	srand (time(NULL));
	double a[N];

	for (int i = 0; i < N; ++i)
	{
		/* random numbers */
		a[i] = rand();
	}

	/* Input for the graph */
	int nnodes=NODES;
	int index[NODES] = {3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48};
	int edges[48]={2,3,4,1,7,11,1,9,13,1,5,15,4,6,10,5,7,12,2,6,8,7,9,14,
		3,8,10,5,9,16,2,12,16,6,11,13,3,12,14,8,13,15,4,14,16,10,11,15};

	/* Start parallel part */
	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    MPI_Graph_create(MPI_COMM_WORLD, nnodes, index, edges , reorder, &graph_comm);
    MPI_Barrier(graph_comm);
	
	/* Getting the benchmark results */    
    for (int i = 0; i < RUNS; ++i)
    {
    	mpi_bcast_time -= MPI_Wtime();
	    MPI_Bcast(a, N, MPI_DOUBLE, 0, graph_comm);
    	MPI_Barrier(graph_comm);
    	mpi_bcast_time += MPI_Wtime();
    }

    mpi_bcast_time = mpi_bcast_time/RUNS;
    
    if (rank == 0)
    	cout << "Time for MPI_Bcast: " << mpi_bcast_time << endl;

	/* Getting the test results */
	for (int i =0; i<RUNS; ++i)
	{
	    my_bcast_time -= MPI_Wtime();
    	my_bcast(a, N, MPI_DOUBLE, 0, graph_comm);
    	MPI_Barrier(graph_comm);
    	my_bcast_time += MPI_Wtime();
	}

	my_bcast_time = my_bcast_time/RUNS;
    
    if (rank == 0)
    	cout << "Time for MY_Bcast: " << my_bcast_time << endl;

    if (rank == 0)
    {
    	ofstream myfile;
      	myfile.open ("output.txt",  std::ofstream::out | std::ofstream::app);
      	myfile << " Avg Time (for " << RUNS <<" runs) for MPI_Bcast : " << mpi_bcast_time << " , num_elements : " << N 
      		<< " , num_processes : " << nproc << endl;
      	myfile << " Avg Time (for " << RUNS << "runs) for My_Bcast : " << my_bcast_time << " , num_elements : " << N 
      		<< " , num_processes : " << nproc << endl;      		
      	myfile.close();
    }
    
    MPI_Finalize();
	return 0;
}