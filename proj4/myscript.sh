#!/bin/bash
#PBS -l nodes=1:ppn=24,walltime=00:30:00
#PBS -N my_example_job
#PBS -q debug 
module load shared
module load mvapich2/gcc/64/2.1
module load blas/gcc/64/3.5.0
cd $PBS_O_WORKDIR

mpirun -np 16 ./myexec 1000000